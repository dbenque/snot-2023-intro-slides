#!/bin/bash

pandoc slides.md \
-t revealjs -s \
-o index.html \
--citeproc \
--metadata pagetitle="IDS @ SNoT 2023" \
--bibliography /home/david/Library/Zotero_current.bib \
--csl harvard-cite-them-right-SLIDE.csl \
-c styles/style.css \
-V revealjs-url=reveal.js \
-V controls=true \
-V theme=black \
-V transition=none \
-V slideNumber=\"c/t\" \
-V backgroundTransition=none \
-V autoPlayMedia=true