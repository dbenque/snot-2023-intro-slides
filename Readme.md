## Installation


## Watch for changes

```bash  
ls *.md | entr ./build.sh
```  

## add .r-stretch class to images

In Codium/VSCode, replace  

```regex  
(!\[.*\]\(.*\))[\n\r\s]  
```  

with   

```  
$1{.r-stretch}\n  
```  


## Features 

### Citations

Use pandoc markdown syntax `@key2020`   

Link to .bib file  
Attached .csl file to format the citation. Uses an author date style but only renders full citations.   