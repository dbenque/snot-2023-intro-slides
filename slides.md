  

# UX Track intro {data-background="slides.assets/silva.jpg" data-background-opacity="0.7"}

---  


## Education

![](slides.assets/DB-cv-meme.png){.r-stretch}

::: notes
3 step CV
- BA: using technology in design practice 
  - dutch, python, type design
- MA: taking technology as the subject of practice
  - using speculation for this
- PhD: taking speculation itself as the subject of research and practice 
:::

---

![](slides.assets/Malife_logos.svg){.r-stretch}  

::: notes  

:::  



# Institute of Diagram Studies

---

![](slides.assets/IDS_logo.svg){.r-stretch}  

https://diagram.institute  

::: notes  

PhD stuff --> diagrams and algorithmic prediction  

since then continued research using the same diagrammatic approach but expanding the remit

- diagrams are a way of seeing/moving/framing/filtering 
- everything can be framed as/with diagrams 
- a language, a set of methods, etc...

Fairly recent: Sept. 2020  

:::  

---  

![](slides.assets/diagram-mediation.png){.r-stretch}  

::: ref  
@benque2020a
:::  

::: notes  
the summary of the Diagram Studies approach 
look at everything through the lens of diagrams
consider relations, mediations, systems, operations

a constraint + a very elastic idea

:::  

--- 

> It is not a question, however, of two fundamentally different types of diagram; rather, this <mark>oscillation between systematising and openness</mark> is inherent in the diagram.

:::ref
@leeb2017
:::

:::notes
oscilations = both in each diagram all the time

retrospective: a way to organise and systematize 
projective: vectors pointing in unknown directions

:::

----  

Operative Images  

> These are images that do not represent an object, but rather are part of an operation. 

::: ref  
@farocki2004  
:::  

::: notes  
not visual studies

a lot of this work is visual, but it doesn't stop there
diagrams extend far beyond just visualisations and that's what gives them purchase and relevance, 

esp. with algorithmic or computational systems

a research practice concerned with operational images.  

instead, diagrams can be described as operative images.   

not about describing or representing but thinking about the larger sets of relations.   

:::  

---  

![](slides.assets/568e7e6bb5f559f4.jpg){.r-stretch}  

:::ref   
Posted: July 26, 2019  
[https://post.lurk.org/\@air_pump/102506785113433897](https://post.lurk.org/\@air_pump/102506785113433897)  
:::  

::: notes  

made this roadmap in [July 26, 2019, 9:23 AM ](https://post.lurk.org/@air_pump/102506785113433897)  

::::  



----  

![](slides.assets/image-20210523223759282.png){.r-stretch}  

::: ref  
[https://www.are.na/institute-of-diagram-studies/index](https://www.are.na/institute-of-diagram-studies/index)  
:::  

---  

![](slides.assets/image-20210523222642952.png){.r-stretch}  

:::ref   
[https://git.diagram.institute]([https://git.diagram.institute])  
:::  

:::: notes  

self hosted infrastructure  

::::   

---

## Projects

:::notes  

diagrams to read the algorithmic apparatus of control   

also speculative devices that can generate new possibilities  

:::  

---  

> We do not react like those who reject [statistics] wholesale and shout: ”No to quantification! No to numbers! Yes to qualities!” because, in doing so, they leave a monopoly over these instruments to the powerful. There is no reason for quantification to always be on the side of the state and of capital.  

:::ref  
@didier2013 my translation  
:::  

:::notes  
counter practice is not a rejection of algorithmic systems  
quote the contrary, it is crucial to engage with them otherwise we give up their control to power and capital.   

different strategies, practices, or personas to embody as a researcher  

examples of thinking and practising with diagrams  

:::  

---  

### Sleuthing

::: notes  
mapping / plotting  

investigation with one's own means  

::::    

---  

> Identifying topoi, analyzing their trajectories and transformations, and explaining the cultural logics that condition their “wanderings” across time and space is one possible goal for media archaeology.  

::: ref  
@huhtamo2011a  
:::  

::: notes  
my research looks a lot to media archaeology for methods and framing

one good concept that moves diagrammatically is this idea of following lineages 


topoi: tropes, threads   

diagrammatic form for the **genealogy** of technology  
:::  


---  

![](slides.assets/1564423218081.png){.r-stretch}  

:::ref  
Diagrams of the Future [DOTF.XYZ](http://dotf.xyz)  
:::  

:::notes  
that is what I set out to do with diagrams of the future, an ongoing mapping of the history of algorithmic prediction through its diagrams.   
:::  

----  

![](slides.assets/1564424028869.png){.r-stretch}  

:::ref  
Diagrams of the Future [DOTF.XYZ](http://dotf.xyz)  
:::  

:::notes  
this tool looks like any other blog on the face of it but it is powered by a graph   

invites putting things in relation and constructing genealogies   

:::  

---  

![](slides.assets/dotf-timeline.png){.r-stretch}  

:::ref  
Diagrams of the Future [DOTF.XYZ](http://dotf.xyz)  
:::  

:::notes  
the result is a bit messy but it counteracts any idea that history, and in this case especially the history of techonolgy is a linear process. instead it is conducive to making more connections, readings and re-readings  

:::  

----  

![](slides.assets/tumblr_o16n2kBlpX1ta3qyvo1_1280.jpg){.r-stretch}  

:::notes  
in this mode, sleuthing is not a definitive act, it does not result in a clear picture of the whodunnit.   

instead it is an ongoing process of piecing together, and of not completely adding up   

home brewed software to conduct the investigation: reflects/informs the knowledge created  

graph database   


continually making and re-making the software  

it acknowledges its own subjectivity and perhaps its own delusions, but it also begins to enable some kind of orientation and reclaiming.   

MORE OF THIS IN HTE NEXT SLIDE DECK

:::  

---  

### Probing

:::notes  
mapping might use some computational techniques but it stays at a distance.   

the next posture I want to propose is one that moves closer to algorithmic systems  
:::  

----  

>  how does one study someone who doesn’t want to be studied and (unlike Spotify’s users) has the power to enforce that wish?  

:::ref  
@james2019  
:::  

:::notes  

and again there are some relational politics here that could be expressed as diagrams.   

the question of access   
more and more limited  
reverse engineering  

:::  

---  

> 'hunters, [...] by interpreting a series of traces reconstruct the appearance of an animal they have never seen'  

:::ref  
@ginzburg1980  
:::  

:::notes  

Traces: types of data collected by YouTube and Netflix to maximise time spent on the site  


the key here is not to focus only on access  
but to embrace a conjectural model  
:::  



---  

![](slides.assets/1571864130260.png){.r-stretch}  



:::ref  
Architectures of Choice: Vol.1 YouTube   
[https://davidbenque.com/projects/architectures-of-choice/](https://davidbenque.com/projects/architectures-of-choice/)  
:::  

:::notes  
probes to generate traces   

:::  

---  

![](slides.assets/2019-01-21-12-11-06.png){.r-stretch}  

:::ref  
Architectures of Choice: Vol.1 YouTube   
[https://davidbenque.com/projects/architectures-of-choice/](https://davidbenque.com/projects/architectures-of-choice/)  
:::  

:::notes  

:::  

-----  


<video stretch controls autoplay loop>   
<source src="slides.assets/screencast_trace.mp4" type="video/mp4">  
</video>  

:::ref  
Architectures of Choice: Vol.1 YouTube   
[https://davidbenque.com/projects/architectures-of-choice/](https://davidbenque.com/projects/architectures-of-choice/)  
:::  

:::notes  

invite speculation as to what the relation between the videos might be  

:::  

---  

![](slides.assets/Screenshot from 2019-10-08 08-47-48.png){.r-stretch}  

:::ref  
Atlas des Recommandations [https://adr.diagram.institute](https://adr.diagram.institute/)  
:::  

:::notes  
workshop continuing this work   
spreading it to other platforms   
:::  

---  

![](slides.assets/Injection Lorette.jpg){.r-stretch}  

::: ref  
*Fluo-Green project*, Université de Namur   
https://sites.google.com/site/fluogproject/  
:::  

:::notes  
probing   
releasing tracing agents to produce traces  

from there imagining algorithms that we will never see   
:::  

---  

### Divining

:::notes  
finally the last activity or practice that I'd like to propose is that of the diviner.   
This completes the trajectory that started at a distance, moved up to probe outputs, to finally the operations themselves and the production of predictions  

:::  

---  

> Chicane comes to signify both an element within symbolic and divinatory performance, and the game of interpretation for both insider and outsider concerning the mysterious phenomena involved.  

:::ref  
@cornelius2016  
:::  

:::notes  
the chicane refers to a trick or sharp turn  
it is often used to describe the sharp turns on a race track   

used by cornelius to theorise the performance of divination and the kinds of trick that the diviner and client partake in to produce the prediction   
:::  

----  

![](slides.assets/diagram-chicane.png){.r-stretch}  

:::ref  

:::  

:::notes  
the chicane may or may not involve an object, such as a crystal ball, or a computer   

In any case thinking about the chicane focuses the process of mediation at play in prediction   

:::  

---  

![](slides.assets/data-pipeline.png){.r-stretch}  

:::ref  
@oneil2013  
:::  

:::notes  
the data pipeline I showed you earlier is in fact a chicane  
the question is is it a sincere one?   

- amazingly, data and algorithms are often presented as unmediated ways to get to the truth, so the fact that the chicane is not acknowledged is not a good sign 
- also to put it in Rameys words the bluff here often involves the diviner pretending to know something that the client doesn't, and doesn't always have their best interest in mind 

:::  

---  

![](slides.assets/almanac-computer.png){.r-stretch}  

:::ref  
almanac.computer [https://almanac.computer](https://almanac.computer)  
:::  

:::notes  
I've explored algorihtmic prediction as divination in a project called almanac computer.   

This project revisits almanac publications, an early prototype of data analytics   
:::  

---  

![](slides.assets/cosmic-commodity.png){.r-stretch}  

:::ref  
Cosmic Commodity Charts - [https://almanac.computer](https://almanac.computer)  
:::  

:::notes  
predicting prices on commodity futures markets from the positions of the planets in the solar system.   
:::  

---  

![](slides.assets/Instagram Post - 2.png){.r-stretch}  

:::ref  
Cosmic Commodity Charts - [https://almanac.computer](https://almanac.computer)  
:::  

:::notes  
this involves quite a bit of chicanery to use the tools of algorithmic prediction at the service of completely different rationalities.   

In doing so my goal was to question which modes of speculation are deemed legitimate and which aren't   
:::  


---

### Operational loops of COVID-19

![](slides.assets/original_4eb8d58a5a84e9a1e60add3ba933cf6c.png){.r-stretch}  

::: ref  

*Operational Loops of COVID-19*   

diagrams for: @sampson2021  

:::  

::: notes  

since Phd  

extending remit of diagrams   

:::   

---

![picture 1](slides.assets/covid-waves.png){.r-stretch}

:::ref
@helmreich2020
::::

---

![picture 2](slides.assets/covid-models.png){.r-stretch}

:::notes
reflection on models 

:::

---  

### Webcam Manager

![](slides.assets/image-20210523230741481.png){.r-stretch}  

:::  ref  

[*Webcam Manager*](https://wm.diagram.institute)  

:::  

::: notes  

more applied and more directly intervening in power relations  

Telecommuters  RYBN and others

Webcam Manager: simple looping of a *gaze* that we have all experienced the zoom call  

::::  

---

![picture 3](slides.assets/wm-diagram.png){.r-stretch}  

:::  ref  
[*Webcam Manager*](https://wm.diagram.institute)  
:::  

---  

![picture 4](slides.assets/wm-ui.png){.r-stretch}  

:::  ref  
[*Webcam Manager*](https://wm.diagram.institute)  
:::  

---

<iframe class="r-stretch" src="https://wm.diagram.institute"></iframe>

:::notes
extensive documentation

current: 
- chicane alignment chart
- IDS website
:::



# Cryptpad

---

## {data-background-color="#424242"}

![CryptPad logo](slides.assets/cryptpad_logo.svg){.r-stretch}

Collaborative office suite,  
End-to-end encrypted and open-source

:::notes

:::

---

## {data-background-color="#eee"}

![venn diagram collaboration and privacy](slides.assets/venn-01.png){.r-stretch}

---

## {data-background-color="#eee"}

![venn diagram collaboration and privacy](slides.assets/venn-02.png){.r-stretch}

---

## {data-background-color="#424242"}

![shredder](slides.assets/cp-shredder.png){.r-stretch}

---

## {data-background-color="#424242"}

![screenshot of the CryptPad drive in dark mode](slides.assets/drive_dark.png){.r-stretch}

---

## {data-background-color="#424242"}

![](slides.assets/cascade_1920x1270-dark.png){.r-stretch}

---

## {data-background-color="#424242"}

![](slides.assets/app-code.png){.r-stretch}

---

## {data-background-color="#424242"}

![](slides.assets/app-rich.png){.r-stretch}

---

## {data-background-color="#424242"}

![](slides.assets/app-kanban.png){.r-stretch}

---

## {data-background-color="#424242"}

![](slides.assets/app-forms.png){.r-stretch}

---

## {data-background-color="#424242"}

![](slides.assets/app-sheet.png){.r-stretch}

---

## {data-background-color="#424242"}

![](slides.assets/app-document.png){.r-stretch}

---

## {data-background-color="#424242"}

![](slides.assets/app-presentation.png){.r-stretch}

---

## {data-background-color="#424242"}

![](slides.assets/app-whiteboard.png){.r-stretch}

---

## {data-background-color="#424242"}

![](slides.assets/app-mdslides.png){.r-stretch}

---

![diagram of all the components of my job as designer of open-source software](slides.assets/Design_role_inv.png){.r-stretch}

:::notes

different roles 
started as design but covers a lot more
now full production of software
  
:::




# Fediverse

---

<div class="social-logos">
​<i class="fa fa-mastodon" aria-hidden="true"></i> 
​<i class="fa fa-pixelfed" aria-hidden="true"></i> 
​<i class="fa fa-peertube" aria-hidden="true"></i>
</div>

\@cryptpad@fosstodon.org

\@cryptpad_design@pixelfed.social

\@cryptpad@peertube.xwiki.com


---

![Lurk logo](slides.assets/lurk-logo.svg){.r-stretch}

Lurk.org  
\@air_pump@post.lurk.org

:::notes
personal use 

Hometown, form of mastodon by Darius Kazemi
:::

---

## A Traversal Network of Feminist Servers

```
– https://hub.vvvvvvaria.org/rosa/ATNOFS/
– https://www.ooooo.be/atraversalnetworkoffeministservers/
– https://systerserver.net/ATNOFS/
– https://txt.lurk.org/ATNOFS/
– https://zoiahorn.anarchaserver.org/ATNOFS/
– https://hypha.ro/ATNOFS
– https://varia.zone/ATNOFS
– https://bleu255.com/~marloes/txts/ATNOFS/
– https://atnofs.constantvzw.org/
– https://psaroskalazines.gr/zines/ATNOFS/
– https://esc.mur.at/en/werk/atnofspublication
```

---

## {data-background-color="#212121"}

![Counter Cloud Action sticker](slides.assets/counter_cloud_action.svg){.r-stretch}

International Trans★Feminist Digital Depletion Strike - 8 March 2023

https://diagram.institute/8m/

---

## Diagramstagram



# Track

---

## 🧠 & 💪 

:::notes
brain & muscle
- each session we can discuss a reading or reference
- then reflect on how it might inform the research or projects that you are working on
- adjust according to needs: e.g. good to step out and consider your projects from a new angle, but it can also be weird to spend time on something unrelated if you have imperatives to follow on your projects
:::

---

## Sessions

---

#### Open Source Design

![flosssss.png](./slides.assets/flosssss.png){.r-stretch}

:::notes

- I'll teach using open-source tools 
  - not assuming that anyone is paying for Adobe Cloud extortion 

- practicing design with open source software is possible now
    - interesting constraint: it's better and cheaper but also forces us to consider the politics of software, lock-ins, etc for ourselves before imposing these dynamics on others who use our products
  
- practices from software development are valuable to design
  - version control

- I believe your apps should be open source 
- how to make something in a considerate way so that it is maintainable
- exit to the community
  - not a plan but a reality from day one

:::

---
 
![penpot](slides.assets/penpot.png){.r-stretch}

:::notes
now is a great time with Penpot

:::

---

#### Map and Territory

![baran-network.png](slides.assets/baran-network.png){.r-stretch}

---

#### Shit user stories

![FrNhIzRWcAEJH85.jpeg](./slides.assets/FrNhIzRWcAEJH85.jpeg){.r-stretch}

---

#### Verrit Archaeology

![verrit.png](./slides.assets/verrit.png){.r-stretch}

---

#### Bleak Futures

![paypal red flag](./slides.assets/paypal red flag.png){.r-stretch}

---


## FIN



![](slides.assets/IDS_logo.svg){.r-stretch}

David Benqué  
[https://diagram.institute](https://diagram.institute)  

<i class="fa fa-mastodon" aria-hidden="true"></i> [\@air_pump@post.lurk.org](https://post.lurk.org/@air_pump)  




